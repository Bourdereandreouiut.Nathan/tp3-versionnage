import box
import user 

class Room:
    def __init__(self,ID,user,capacity=None):
        self._contains = []
        self._ID = ID
        self.user = user
        self._capacity = capacity
    def getId(self):
        return self._ID
    def getuser(self):
        return self.user
    def getlist(self):
        return self._contains 
    def getcapacity(self):
        return self._capacity
    def setId(self,ID):
        self._ID = ID 
    def setuser(self,user):
        self._user = user
    def setcapacity(self,capacity):
        self._capacity = capacity
    def contains(self,box):
        for the_box in self._contains:
            if the_box==box:
                return True
        return False
    def add_in_room(self,box):
        if self.has_capacity_for(box) or self._capacity == None:
            self._contains.append(box)
        else:
            print("not enough capacity")
            
    
    