import yaml
import io

class Box:
    def __init__(self , is_open=False , capacity=None,name = None ):
        
        self._contents = []
        self._is_open = is_open
        self._capacity = capacity
    
    def __repr__(self):
        return "%s,%s,%s" % (self._contents, self.is_open,self.capacity)

    def __eq__ (self , other):
        return self._contents == other._contents and self.is_open == other.is_open and self.capacity == other.capacity 
    
    @staticmethod
    def from_yaml(data):
        c = data.get(" capacity ", None)
        i = data.get(" is_open " , None)
        ca = data.get(" [] ", None)
        return Box( capacity=c, is_open=i, contents=ca)
    
    def has_room_for(self,o):
        
        try:
            total = 0
            if self.get_capacity() == None:
                return True
            elif self.get_capacity() > Objet.volume(o):
                for un_objet in self.get_capacity():
                    total+= Objet.volume(un_objet)
                    if (total + Objet.volume(o)) <= self.get_capacity():
                        return True
                    else:
                        return False
            else:
                return False
        except:
            return "objet non existant"
            
    def add(self , truc ):      
        if self.has_room_for(truc) and self.is_open():
            self._contents.append(truc)
        else:
            return "pas de place !"
    def remove(self,truc):
        try:
            self._contents.remove(truc)
        except :
            return "l'objet mentionné n'est pas dans la boite !"
               
    def __contains__(self,machin):
        
        if machin not in self._contents:
            return False
        return True
    
    def close(self):
        
        self._is_open=False
    
    def open(self):
        
        self._is_open=True
    
    def get_capacity(self):
        
        return self._capacity
    
    def set_capacity(self,cap):
        
        self._capacity = cap
        
    def is_open(self):
        
        return self._is_open
    
    def contents_name(self):
        
        liste = []
        for content in self._contents:
            name = Objet.nom(content)
            liste.append(name)
        return liste
    
    def action_look(self):
        
            if self.is_open() == True:
                return "la boite contient: "+', ' .join(self.contents_name())
            else:
                return "la boite est fermée !"
                
    def find(self,nom):
        res = [t for t in self._contents if Objet.has_nom(t,nom)]
        return res[0] if len(res[0])>0 else None
    
class Objet:
    def __init__(self,poids=1,nom=None):
        
        self._poids = poids
        self._nom = nom
    
    def volume(self):
        
        return self._poids
    
    def set_volume(self,le_poids):
        
        self._poids = le_poids
    
    def nom(self):
        return self._nom
    
    def set_nom(self,le_nom):
        self._nom = le_nom
        
    def has_nom(self):
        if self.nom() == None:
            return None
        return self.nom()
    
    def __repr__(self):
        return "%s,%s" % (self._poids,self._nom)
    
    @staticmethod
    def from_yaml(data):
        p = data.get(" poids ", None)
        n = data.get(" nom " , None)
        return Objet( poids=p, nom=n)

def load_yaml(data):
    ide = 0
    for item in data:
        if item["key"] == "Objet":
            Object = Objet.from_yaml(item)
            Box.add(ide,Object)
        elif item["key"] == "Box":
            ide = int(ide)
            ide+=1
            ide = str(ide)
            ide = Box.from_yaml(item)
        else:
            return "La boite contient: " + ", ".join(self._contents)

    
b = Box()


# b.open()

# b.action_add(t)

# b.action_look()

# un test
