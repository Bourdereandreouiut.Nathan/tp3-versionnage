from box import *
from donnees import *
import yaml
import io

# on veut pouvoir creer des boites
def test_box_create():
	b = Box()
	b.add("truc1")
	b.add("truc2")


def test_remove():
	b = Box()
	b.add("truc1")

	b.retirer("truc1")

	
	with pytest.raises(NoObjectException):
		b.retirer("truc2")
	
def test_is_opened():
	b = Box()
	assert b.is_open() == False

	b.open()
	assert b.is_open() == True

	b.close()
	assert b.is_open() == False

def test_action_look():
	b = Box()
	b.add("ceci")
	b.add("cela")

	assert b.action_look() == "La boite est fermée"

	b.open()

	assert b.action_look() == "La boite contient: ceci, cela"

def test_vol_thing():
	t = Thing(3)

	assert t.volume() == 3


def test_action_add():
	b = Box()
	t = Thing(3)

	b.open()

	b.action_add(t)

#on veut pouvoir créer des boites
def test_box_create():
    b=Box()
# on veut pouvoir mettre des trucs dedans
def test_box_add ():
    b = Box ()
    b.add("truc1")
    b.add("truc2")
    
def test_box_open():
    b = Box()
    assert not b.is_open()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()
def test_action_look():
    b=Box()
    o1 = Objet(poids=2,nom="truc")
    o2 = Objet(poids=2,nom="machin")
    assert b.action_look() == "la boite est fermée !"
    b.open()
    b.add(o1)
    b.add(o2)
    assert b.action_look() == "la boite contient: truc, machin"

def test_remove():
    b = Box()
    o1 = Objet(poids=2,nom="truc")
    o2 = Objet(poids=2,nom="machin")
    o3 = Objet(poids=1,nom="bidule")
    if b.is_open() == False:
        b.open()
    b.add(o1)
    b.add(o2)
    b.remove(o2)
    assert o1 in b
    assert o2 not in b
    assert b.remove(o3) == "l'objet mentionné n'est pas dans la boite !"

def test_room_for():
    b = Box()
    o = Objet()
    b.add("truc")
    b.add("machin")

def test_objet():
    o = Objet()
    assert o.nom() == None
    o.set_nom("Epee")
    assert o.nom() == "Epee"
    assert o.volume() == 1
    o.set_volume(3)
    assert o.volume() == 3

text = """
- key : Box
  is_open : true
  capacity : 3
- key : Objet
  poids : 3
  nom : bonsoir
- key : Erreur
  is_open : false
  capacity : 5
"""

def test_yaml():
    stream = io.StringIO(text)
    l = yaml.load(stream)
    o = Objet()
    b = Box()
    print(l)
print(test_yaml)    