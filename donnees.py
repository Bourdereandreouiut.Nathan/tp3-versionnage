import yaml
import io 

text = """
- key : Box
  is_open: true
  capacity: 3
- key : Objet
  poids : 3
  nom : bonsoir
- key : Erreur
  is_open: false
  capacity: 5
"""
stream = io.StringIO(text)
l = yaml.load(stream)
print(l)